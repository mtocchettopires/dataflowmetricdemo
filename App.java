
import com.google.cloud.monitoring.v3.MetricServiceClient;
import com.google.cloud.monitoring.v3.MetricServiceClient.ListTimeSeriesPagedResponse;
import com.google.monitoring.v3.ListTimeSeriesRequest;
import com.google.monitoring.v3.ListTimeSeriesRequest.TimeSeriesView;
import com.google.monitoring.v3.ProjectName;
import com.google.monitoring.v3.TimeInterval;
import com.google.monitoring.v3.TimeSeries;

import com.google.protobuf.util.Timestamps;


public class App {

  public static void main(String... args) throws Exception {
    String projectId = System.getenv("GOOGLE_CLOUD_PROJECT");

    MetricServiceClient metricServiceClient = MetricServiceClient.create();
    ProjectName name = ProjectName.of(projectId);

    /*
      An interval is always required, since this particular metric, Elapsed time, is of type GAUGE, it only need an end time
      and the start time will be the same. The problem is that in order to get the information, we must specify the EXACT
      end time, or an interval that contain the metric, for example star time 1 AM and end time 23:59 PM, the whole day. But then
      that will return several time series as this elapsed time was mesured a couple times before the job ended. This seems to be the best option
      since otherwise we would first have to (somehow) query for the end time, hope it is with the same offset (The job ended at 12:59 but in order to get this
      metric we have to set the end time to 19:01, no idea why), and then use that end time to get this query, and this is only ONE metric.
     */
    TimeInterval interval = TimeInterval.newBuilder()
        .setEndTime(Timestamps.parse("2018-06-29T19:01:01.005Z"))
        .build();


    /*
    First argument, metric.type, is the "end point" and the second is the arguments that end point need to, in this case job_id
     */
    ListTimeSeriesRequest.Builder requestBuilder = ListTimeSeriesRequest.newBuilder()
        .setName(name.toString())
        .setFilter("metric.type=\"dataflow.googleapis.com/job/elapsed_time\" AND metric.label.job_id=\"2018-06-29_11_53_34-10721391837718852402\"")
        .setInterval(interval)
        .setView(TimeSeriesView.FULL);

    ListTimeSeriesRequest request = requestBuilder.build();

    ListTimeSeriesPagedResponse response = metricServiceClient.listTimeSeries(request);

    System.out.println("Got timeseries metricss: ");
    for (TimeSeries ts : response.iterateAll()) {
      System.out.println(ts);
    }


  }

}
